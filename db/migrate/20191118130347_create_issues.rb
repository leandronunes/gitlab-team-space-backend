class CreateIssues < ActiveRecord::Migration[6.0]
  def change
    create_table :issues do |t|
      t.column :issue_id, :integer
      t.column :issue_iid, :integer
      t.column :project_id, :integer
      t.column :title, :string
      t.column :description, :text
      t.column :label, :string
      t.column :web_url, :string
      t.column :state, :string
      t.column :opened_at, :datetime
      t.column :opened_at_year_week, :integer
      t.column :initiated_at, :datetime
      t.column :initiated_at_year_week, :integer
      t.column :initiated_by, :integer
      t.column :closed_at, :datetime
      t.column :closed_at_year_week, :integer
      t.column :closed_devel_at, :datetime
      t.column :closed_devel_at_year_week, :integer
      t.column :author_id, :integer
      t.column :subscriber_id, :integer
      t.column :last_updated_at, :datetime
      t.column :lead_time, :integer

      t.timestamps
    end
  end

end
